import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NplSearchPageComponent } from './npl-search-page.component';

describe('NplSearchPageComponent', () => {
  let component: NplSearchPageComponent;
  let fixture: ComponentFixture<NplSearchPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NplSearchPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NplSearchPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
