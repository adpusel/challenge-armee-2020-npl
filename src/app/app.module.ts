import { BrowserModule } from '@angular/platform-browser';
import { NgModule }      from '@angular/core';

import { AppRoutingModule }                                                         from './app-routing.module';
import { AppComponent }                                                             from './app.component';
import { BrowserAnimationsModule }                                                  from '@angular/platform-browser/animations';
import { DisplayResultComponent }                                                   from './components/display-result/display-result.component';
import { NplSearchPageComponent }                                                   from './containers/npl-search-page/npl-search-page.component';
import { SearchBarComponent }               from './components/search-bar/search-bar.component';
import {
  MatFormFieldModule,
  MatIconModule,
  MatAutocompleteModule,
  MatChipsModule,
  MatInputModule,
  MatCardModule,
  MatButtonModule, MatSnackBarModule
} from '@angular/material';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { LayoutModule }                     from '@angular/cdk/layout';
import { FlexLayoutModule }                 from '@angular/flex-layout';

@NgModule( {
  declarations : [
    AppComponent,
    DisplayResultComponent,
    NplSearchPageComponent,
    SearchBarComponent
  ],
  imports : [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FormsModule,
    MatInputModule,
    MatFormFieldModule,
    MatIconModule,
    MatAutocompleteModule,
    ReactiveFormsModule,
    MatChipsModule,
    MatCardModule,
    MatButtonModule,
    FlexLayoutModule,
    MatSnackBarModule
  ],
  providers : [],
  bootstrap : [ AppComponent ]
} )
export class AppModule {}
