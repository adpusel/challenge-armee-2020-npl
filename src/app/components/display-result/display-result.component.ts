import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { MatSnackBar }                                from '@angular/material';

@Component( {
  selector : 'app-display-result',
  template : `
    <mat-card *ngFor="let el of fakeData">
      <mat-card-header>
        <div mat-card-avatar class="avatar"></div>
        <mat-card-title>{{ el.file }}</mat-card-title>
        <mat-card-subtitle>{{ el.author }}</mat-card-subtitle>
      </mat-card-header>
      <mat-card-content *ngFor="let text of el.data">
        <div fxLayout fxLayoutAlign="space-between center">
          <h5>page : {{text.page }}</h5>
          <button mat-button color="primary" (click)="addElement()">Ajouter</button>
        </div>
        <p>
          {{ text.text }}
        </p>
      </mat-card-content>
    </mat-card>
  `,
  styles : [ `
    .avatar {
      background-image: url("https://www.rd.com/wp-content/uploads/2017/09/01-shutterstock_476340928-Irina-Bg-1024x683.jpg");
      background-size: cover;
      background-position: center;
    }

    mat-card-content {
      padding: 0 20px;
    }
  ` ],
  changeDetection : ChangeDetectionStrategy.OnPush
} )
export class DisplayResultComponent implements OnInit {

  fakeData = [
    {
      file : 'super fichier 1', author : 'Jeam-michel de la foret', date : new Date(),
      data : [
        {
          page : '23',
          text : 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.'
        }, {
          page : '23',
          text : 'standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.'
        }, {
          page : '23',
          text : 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.'
        }
      ]
    }, {
      file : 'super fichier 2', author : 'Jeam-michel de la foret', date : new Date(),
      data : [
        {
          page : '23',
          text : 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.'
        }
      ]
    },
    {
      file : 'super fichier 2', author : 'Jeam-michel de la foret', date : new Date(),
      data : [
        {
          page : '23',
          text : 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.'
        }, {
          page : '23',
          text : 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.'
        }
      ]
    }

  ];

  constructor( private snackBar: MatSnackBar ) { }

  addElement() {
    this.snackBar.open( 'L\'element a bien ete ajoute', undefined, { duration : 2000 } );
  }

  ngOnInit() {
  }

}
